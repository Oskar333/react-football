import React from 'react';
import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';
import Competitions from './components/Competitions';
import Teams from './components/Teams';
import Players from './components/Players';
import './App.css';

const App = () => (
  <Router>
    <div>
      <Route exact path="/" component={Competitions}/>
      <Route path="/teams" component={Teams}/>
      <Route path="/players" component={Players}/>
    </div>
  </Router>
)






export default App;
