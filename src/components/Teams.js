import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Teams extends Component {

    constructor(props, context) {
        super();
        this.state = {
            teams: []
        };

        this.instance = axios.create({
            baseURL: 'http://api.football-data.org/v1',
            headers: {'X-Auth-Token': 'd1ea93deca93418094d3b678b33e4055'}
        });

    }

    getTeams(id) {
        this.instance.get('/competitions/' + id + '/teams')
        .then((teams) => {
            console.log(teams.data.teams);
            this.setState({
                teams: teams.data.teams
            });
        })
        .catch((error) => {
            console.log(error);
        });
       
    }

    componentDidMount() {
        console.log(this.props.location.state.id);
        this.getTeams(this.props.location.state.id);
    }

    render() {
        return (
            <div>
            <h1 className="text-center">Teams</h1>
            <div className="list-group">
                {this.state.teams.map((team) => {
                    return(
                        <Link to={{pathname: "/players", state:{id: team._links.players.href}}} key={team.code} className="list-group-item list-group-item-action">
                            <img className="d-flex align-self-center mr-3" src={team.crestUrl} alt="..."/>{team.name}</Link> 
                    )
                })}
            </div>
            </div>
        )
    }
}
export default Teams;