import React, { Component } from 'react';
import axios from 'axios';


class Players extends Component {

    constructor(props, context) {
        super();
        this.state = {
            players: []
        };

        this.instance = axios.create({
            baseURL: 'http://api.football-data.org/v1',
            headers: {'X-Auth-Token': 'd1ea93deca93418094d3b678b33e4055'}
        });

    }

    getPlayers(id) {
        this.instance.get('/teams/' + id + '/players')
        .then((players) => {
            console.log(players.data);
            this.setState({
                players: players.data.players
            });
        })
        .catch((error) => {
            console.log(error);
        });
       
    }

    componentDidMount() {
        console.log(this.props.location.state.id);
        var str = this.props.location.state.id;
        var res = str.split("/");
        console.log(res);
        this.getPlayers(res[5]);
    }

    render() {
        return (
            <div>
                <h1 className="text-center">Players</h1>
                <table className="table">
                    <thead>
                        <tr>
                        <th>Number</th>
                        <th>Name</th>
                        <th>Nationality</th>
                        <th>Birth</th>
                        <th>Position</th>
                        <th>Contract</th>
                        <th>Value</th>
                        </tr>
                    </thead>
                    <tbody>                   
                        {this.state.players.map((player) => {
                            return(
                                <tr key={player.dateOfBirth + player.jerseyNumber}>
                                    <td>{player.jerseyNumber}</td>
                                    <td>{player.name}</td>
                                    <td>{player.nationality}</td>
                                    <td>{player.dateOfBirth}</td>                                   
                                    <td>{player.position}</td>
                                    <td>{player.contractUntil}</td>
                                    <td>{player.marketValue}</td>
                                </tr>
                            )                        
                        })}                                                          
                    </tbody>
                </table>
            </div>
        )
    }
}
export default Players;