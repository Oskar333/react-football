import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

class Competitions extends Component {

    constructor(props, context) {
        super();
        this.state = {
            competitions : []
        };

        this.instance = axios.create({
            baseURL: 'http://api.football-data.org/v1',
            headers: {'X-Auth-Token': 'd1ea93deca93418094d3b678b33e4055'}
        });

    }

    fetchData() {
        this.instance.get('/competitions/?season=2017')
        .then((competitions) => {
            console.log(competitions.data);
            this.setState({
                competitions: competitions.data
            });
        })
        .catch((error) => {
            console.log(error);
        });
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        return (
            <div>
            <h1 className="text-center">Competitions</h1>
            <div className="list-group">
                {this.state.competitions.map((competition) => {
                    return(
                        <Link to={{pathname: "/teams", state:{id: competition.id}}} key={competition.id} className="list-group-item list-group-item-action">{competition.caption}</Link>
                    )
                })}
            </div>
            </div>
        )
    }
}
export default Competitions;